import { Injectable } from '@angular/core';
import { Books } from "../models/books";
import { of, Observable } from "rxjs";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { map } from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class BooksService {
   //
   private urlEndPoint: string = 'https://api-tester.uniko.co';
   private listBooks;
   // Create attribute books.
   public books: Books = new Books();

  constructor(private http: HttpClient) { }

  httpOptions = { headers: new HttpHeaders({ "Content-type": "application/json"})}

   // Get clientes.
   getBooks() {
      // Safe return.
      return this.http.get(`${this.urlEndPoint}/books`).subscribe(resp => {
        // Asign list book response
        this.listBooks = resp;
      });
   }

   // Create books.
   createBook(book: Books) {
      // Create book.
      return this.http.post<Books>(`${this.urlEndPoint}/books`, book)
      .pipe(map( data => data ));
   }

   // Delte book.
   deleteBook(id) {
      // Delete book.
       return this.http.delete<Books>(`${this.urlEndPoint}/books/${id}`)
       .pipe(map( data => data ));
   }

   // Update book.
   updateBook(book: Books) {
      // Safe return.
      return this.http.put(`${this.urlEndPoint}/books/${book.id}`, book);
   }
}
