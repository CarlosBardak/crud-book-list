import { Component, OnInit } from '@angular/core';
import { Books } from "../models/books";
import { BooksService } from "../services/books.service";

@Component({
  selector: 'app-list-books',
  templateUrl: './list-books.component.html',
  styleUrls: ['./list-books.component.css']
})
export class ListBooksComponent implements OnInit {

  constructor(private booksService: BooksService) { }

  ngOnInit() {
    // Get list books..
    this.booksService.getBooks();
  }

  // Edit.
  onEditBook(book: Books) {
    // Update from book.
    this.booksService.books = Object.assign({}, book);
  }

  // Delete
  onDelete(id: string) {
    // Delete book on api.
    this.booksService.deleteBook(id).subscribe(data => {
      console.log("Delete Book Success");
      // Reload list books.
      this.booksService.getBooks();
    });
  }
}
