import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormArray } from '@angular/forms';
import { Books } from "../models/books";
import { BooksService } from "../services/books.service";

@Component({
  selector: 'app-form-books',
  templateUrl: './form-books.component.html',
  styleUrls: ['./form-books.component.css']
})
export class FormBooksComponent implements OnInit {
  // Title
  private title:String  = "FORM BOOK";
  private myForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private booksService: BooksService
    ) { }

  ngOnInit() {
      this.myForm = this.fb.group({
         arrayForms: this.fb.array([])
      });
  }

  // Return array form.
  get listForms() {
    //
    return this.myForm.get('arrayForms') as FormArray
  }

  // Add form list.
  addForms() {
    const forms = this.fb.group({
      author: "",
      title: "",
      price: "",
    })
    // Insert for draw.
    this.listForms.push(forms);
  }

  // Delete form.
  deleteFormBook(i) {
    // Reset form.
    this.resetFrom()
    // Remove form.
    this.listForms.removeAt(i);
  }

  // Create book
  createBook() {
    debugger
    // Validate id.
    if ( this.booksService.books.id !== "" ) {
      // Update book.
      this.booksService.updateBook(this.booksService.books).subscribe(res => {
        console.log("Update Book Succsess..");
        // Reload list book.
        this.booksService.getBooks();
      });
    } else {
      // Create books en service.
      this.booksService.createBook(this.booksService.books).subscribe(res => {
        console.log("Create Book success")
        // Reload list book.
        this.booksService.getBooks();
      });
    }

    // Reset form.
    this.resetFrom();
  }

  // Reset form book.
  resetFrom() {
    // Reset form.
    this.myForm.reset()
    //
    this.booksService.books = {
      id: "",
      author: "",
      title: "",
      price: 0,
      description: ""
    }
  }
}
