import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ListBooksComponent } from './list-books/list-books.component';
import { FormBooksComponent } from './form-books/form-books.component';
import { HttpClientModule } from '@angular/common/http';
import { BooksService } from "./services/books.service";

@NgModule({
  declarations: [
    AppComponent,
    ListBooksComponent,
    FormBooksComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    BooksService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
