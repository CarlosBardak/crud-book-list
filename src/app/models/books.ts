export class Books {
  id: string;
  author: string;
  title: string;
  price: number;
  description: string;
}
